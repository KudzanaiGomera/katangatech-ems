import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SnotifyService } from 'ng-snotify';
import { JarwisService } from 'src/app/services/jarwis.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public form = {
    name: null,
    email: null,
    username: null,
    age: null,
    gender: null,
    location: null,
    interest: null,
    avatar: null,
    about: null,
  }

  public avatar: File = null;

  public error = {
    name:null,
    email:null,
    username:null,
    age:null,
    gender:null,
    location:null,
    interest:null,
    avatar:null,
    about: null,
  };

  constructor(
    private Jarwis:JarwisService,
    private router:Router,
    private Token:TokenService,
    private Notify: SnotifyService,
    private notify: SnotifyService,
  ) { }

  onFileSelected(event) {
    this.avatar = <File>event.target.files[0];
    console.log(this.avatar);

  }


  onSubmit() {
    let myFormData = new FormData();
    myFormData.append('avatar', this.avatar);
    myFormData.append('name', this.form.name);
    myFormData.append('username', this.form.username);
    myFormData.append('email', this.form.email);
    myFormData.append('age', this.form.age);
    myFormData.append('location', this.form.location);
    myFormData.append('gender', this.form.gender);
    myFormData.append('interest', this.form.interest);
    myFormData.append('about', this.form.about);

    myFormData.forEach((value, key) => {
        console.log(key+" : "+value)
    });

    this.Notify.info('Completing profile wait...', {timeout:1000})
    this.Jarwis.profile(myFormData).subscribe(
      data => this.handleResponse(data),
      error => this.notify.error(error.error.error)
    )
  }

  ngOnInit(): void {
  }

  handleResponse(data) {
    this.Token.handle(data.access_token);
    this.Notify.success('Profile Completed',{timeout:0})
    this.router.navigateByUrl('/profile');
  }

}
