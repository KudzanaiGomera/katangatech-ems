# !important

- make sure you have laravel installed in your machine
- copy and paste the project folder(ems) to you xampp or mamp htdocs
- create a .env file in app directory
- copy && paste the .env.example
- add this REDIRECT = "http://localhost:4200/login" at the end of .env
- make sure in your database you have the db created eg(larangular)

# Run

- composer update

- php artisan key:generate 

//to generate an application (encryption) key and it is added to the env file automatically.

# Send an email Config

- also remove MAIL_FROM_ADRRESS

- also remove FROM_NAME

- MAIL_MAILER=smtp
- MAIL_HOST=smtp.gmail.com
- MAIL_PORT=587
- MAIL_USERNAME=Youremail@gmail.com
- MAIL_PASSWORD=Youremail_pwd
- MAIL_ENCRYPTION=tls

# Run server

- php artisan migrate

- php artisan serve


