# README #

* The EMS is built with laravel(backend) + Angular(frontend)
* Each framework has it's own read me file navigate to backend first and follow the backend readme file
* then cd in frontend directory and follow the readme file as well

### What is this repository for? ###

* This repo contains an event management system, that will handle all your events you plan to go using cutting edge technology and bring huge  events close to your finger tips
* Laravel(7) + Angular(8)

### How do I get set up? ###

* clone the repo -> git clone https://KudzanaiGomera@bitbucket.org/KudzanaiGomera/katangatech-ems.git
* cd katangatech-ems
* cut the ems directory
* paste it into your htdocs (either Xampp or Mamp)
* open with text editor the ems directory
* check the backend directory first and follow the readme
* then navigate to the frontend directory and folow the readme

### Contribution guidelines ###

* Writing tests -> Kudzanai Gomera
* Code review -> Junior and Sipho
* Other guidelines -> Saskya

### Who do I talk to? ###

* Kudzanai Gomera
* Katanga Dev Team